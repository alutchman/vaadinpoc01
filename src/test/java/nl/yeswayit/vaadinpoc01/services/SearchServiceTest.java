package nl.yeswayit.vaadinpoc01.services;

import nl.yeswayit.vaadinpoc01.controllers.NotFoundMyItemExcpetion;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceTest {

    @InjectMocks
    private SearchService searchService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void handleSerchHappyFlow() {
        String value = searchService.handleSerch("Breakfast");
        assertEquals("tea", value);
    }

    @Test
    public void handleSerchUnHappyFlow() {
        expectedException.expect(NotFoundMyItemExcpetion.class);
        expectedException.expectMessage("Niet gevonde item : MidNight");
        searchService.handleSerch("MidNight");

    }
}