package nl.yeswayit.vaadinpoc01.data.db.repo;

import nl.yeswayit.vaadinpoc01.data.db.tables.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface  repoTodo extends JpaRepository<Todo,Long> {

}
