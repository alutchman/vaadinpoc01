package nl.yeswayit.vaadinpoc01.data.db.tables;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Getter
@Setter
@Entity
public class Authenticate {

    @Id
    private String username;

    private String password;

    private String access;

    private String firstname;

    private String lastname;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Authenticate that = (Authenticate) o;
        return getUsername().equals(that.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLastname());
    }
}
