package nl.yeswayit.vaadinpoc01.data.db.repo;

import nl.yeswayit.vaadinpoc01.data.db.tables.Authenticate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RepoAuthenticate extends JpaRepository<Authenticate, String> {

    @Query("SELECT t FROM Authenticate t where username=?1 AND password=?2")
    Optional<Authenticate> finduser(String username, String password);

    @Query("SELECT t FROM Authenticate t where username=?1")
    Optional<Authenticate> finduser(String username);


    @Query("SELECT t FROM Authenticate t where firstname like ?1 AND lastname like ?2 AND username <> ?3")
    Optional<List<Authenticate>> searchUser(String firstname, String lastname, String usernameExclude);
}
