package nl.yeswayit.vaadinpoc01.views;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@UIScope
@SpringView(name=ErrorView.NAME)
public class ErrorView extends CustomComponent implements View {
    public static final String NAME = "error";

    public ErrorView(){

    }

}
