package nl.yeswayit.vaadinpoc01.views;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.LoginForm;
import com.vaadin.ui.UI;
import nl.yeswayit.vaadinpoc01.VaadinUI;
import nl.yeswayit.vaadinpoc01.services.AuthenticateService;
import org.springframework.beans.factory.annotation.Autowired;



@UIScope
@SpringView(name=LoginView.NAME)
public class LoginView extends LoginForm implements View {

    public final static String NAME = "Login";

    @Autowired
    private AuthenticateService authenticateService;

    public LoginView() {
        initGUI();
    }

    private void initGUI() {
        setWidth("250px");
        this.addLoginListener(event -> {

            String username = event.getLoginParameter("username");
            String password = event.getLoginParameter("password");

            if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
                return;
            }
           handleLogin(username, password);
        });

    }

    private void handleLogin(String username, String password) {
        AuthenticateService authenticateService = ((VaadinUI) UI.getCurrent()).getBean(AuthenticateService.class);
        if(authenticateService.isValid(username, password)){
            // Navigate to application container
            getSession().setAttribute("username",username);
            UI.getCurrent().getNavigator().navigateTo(MainView.NAME);
        }
    }
}
