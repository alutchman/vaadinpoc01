package nl.yeswayit.vaadinpoc01.views;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import nl.yeswayit.vaadinpoc01.VaadinUI;
import nl.yeswayit.vaadinpoc01.services.AuthenticateService;
import nl.yeswayit.vaadinpoc01.services.SearchService;


@UIScope
@SpringView(name=MainView.NAME)
public class MainView extends CustomComponent implements View {

    public final static String NAME = "Main";

    public MainView() {
        initGUI();
    }

    private void initGUI() {

        VerticalLayout content = new VerticalLayout();

        Label lbl = new Label("");
        TextField search = new TextField();
        Button btnSearch = new Button("Opzoeken");

        content.addComponents(search, btnSearch, lbl);

        btnSearch.addClickListener(click -> {
            SearchService searchService = ((VaadinUI) UI.getCurrent()).getBean(SearchService.class);
            lbl.setValue(searchService.handleSerch(search.getValue()));
        });

        setCompositionRoot(content);

    }
}
