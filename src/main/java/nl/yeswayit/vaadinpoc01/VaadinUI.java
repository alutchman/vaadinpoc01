package nl.yeswayit.vaadinpoc01;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.ErrorEvent;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.UI;
import lombok.extern.slf4j.Slf4j;
import nl.yeswayit.vaadinpoc01.config.NavigatorViewChange;
import nl.yeswayit.vaadinpoc01.views.ErrorView;
import nl.yeswayit.vaadinpoc01.views.LoginView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@Theme("valo")
@Slf4j
@SpringUI
public class VaadinUI extends UI {

    @Autowired
    private SpringViewProvider viewProvider;

    @Autowired
    ApplicationContext applicationContext;


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        log.info("Class servlet = {}", VaadinServlet.getCurrent().getClass().getName());

        /*
        Navigator init hoort bij UI
         */
        UI.getCurrent().setNavigator(new Navigator(UI.getCurrent(), UI.getCurrent()));
        UI.getCurrent().getNavigator().addProvider(viewProvider);
        UI.getCurrent().getNavigator().setErrorView(ErrorView.class);

        UI.getCurrent().getNavigator().addViewChangeListener(new NavigatorViewChange() );
        UI.getCurrent().setErrorHandler(new ErrorHandler() {
            @Override
            public void error(com.vaadin.server.ErrorEvent errorEvent) {
                log.error(errorEvent.getThrowable().getMessage());
            }
        });

    }

    public <T> T getBean(Class<T> aClass) {
        return applicationContext.getBean(aClass);
    }





}
