package nl.yeswayit.vaadinpoc01.controllers;

import nl.yeswayit.vaadinpoc01.views.MainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Controller
public class BaseErrorRestcontroller   implements ErrorController {
    private boolean debug = false;

    @Autowired
    private ErrorAttributes errorAttributes;

    @ResponseBody
    @RequestMapping(value = "/error")
    public String error(WebRequest webRequest, HttpServletResponse response) throws IOException {
        response.sendRedirect(new StringBuilder("#!").append(MainView.NAME).toString());
        Map<String, Object> errorInfo = errorAttributes.getErrorAttributes(webRequest, debug);

        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}