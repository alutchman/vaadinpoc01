package nl.yeswayit.vaadinpoc01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vaadinpoc01Application {

    public static void main(String[] args) {
        SpringApplication.run(Vaadinpoc01Application.class, args);
    }
}
