package nl.yeswayit.vaadinpoc01.config;

import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import nl.yeswayit.vaadinpoc01.views.ErrorView;
import nl.yeswayit.vaadinpoc01.views.LoginView;
import nl.yeswayit.vaadinpoc01.views.MainView;


public class NavigatorViewChange implements ViewChangeListener {
    @Override
    public boolean beforeViewChange(ViewChangeEvent viewChangeEvent) {
        // Check if a user has logged in
        boolean isLoggedIn = VaadinSession.getCurrent().getAttribute("username") != null;
        boolean isLoginView = viewChangeEvent.getNewView() instanceof LoginView;
        boolean isErrorView = viewChangeEvent.getNewView() instanceof ErrorView;

        if (!isLoggedIn && !isLoginView) {
            // Redirect to login view always if a user has not yet
            // logged in
            UI.getCurrent().getNavigator().navigateTo(LoginView.NAME);
            return false;

        } else if (isLoggedIn && isLoginView) {
            // If someone tries to access to login view while logged in,
            // then cancel

            //=== Maar toon wel een default view !!! anders staat er een wit scherm zonder fout melding
            UI.getCurrent().getNavigator().navigateTo(MainView.NAME);
            return false;
        } else if (isLoggedIn && isErrorView) {
            UI.getCurrent().getNavigator().navigateTo(MainView.NAME);
            return false;
        }
        return true;
    }
}
