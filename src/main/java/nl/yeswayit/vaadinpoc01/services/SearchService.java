package nl.yeswayit.vaadinpoc01.services;

import lombok.extern.slf4j.Slf4j;
import nl.yeswayit.vaadinpoc01.controllers.NotFoundMyItemExcpetion;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class SearchService {
    private Map<String,String> searchmap = new HashMap<>();

    {
        searchmap.put("Breakfast", "tea");
        searchmap.put("Lunch", "Cola");
        searchmap.put("Dinner", "Beer");
    }

    public String  handleSerch(String value) {
        log.info("You are searching for {}", value);

        if (searchmap.containsKey(value)) {
            return searchmap.get(value);
        }
        throw  new NotFoundMyItemExcpetion("Niet gevonde item : "+ value);
        //return "Niet gevonden";
    }
}
