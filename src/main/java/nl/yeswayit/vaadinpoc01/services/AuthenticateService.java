package nl.yeswayit.vaadinpoc01.services;

import lombok.extern.slf4j.Slf4j;
import nl.yeswayit.vaadinpoc01.data.db.repo.RepoAuthenticate;
import nl.yeswayit.vaadinpoc01.data.db.tables.Authenticate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@Slf4j
@Service
public class AuthenticateService {

    private static final String MAIN_ADMIN = "admin";

    private static final String INIT_PW = "welkom01";

    @Autowired
    private RepoAuthenticate repoAuthenticate;

    @PostConstruct
    private void init() {

        ensureAdmin();

    }

    @Transactional
    protected void ensureAdmin() {

        log.info("Admin registration...");

        Optional<Authenticate> entityOpt = repoAuthenticate.findById(MAIN_ADMIN);
        if (!entityOpt.isPresent()) {

            Authenticate webUser = new Authenticate();
            webUser.setUsername(MAIN_ADMIN);
            webUser.setAccess("ADMIN");
            webUser.setLastname("Applicatie");
            webUser.setFirstname("Beheer");
            webUser.setPassword(createHashData(INIT_PW));
            repoAuthenticate.saveAndFlush(webUser);

        }

    }

    private String createHashData(String input) {

        MessageDigest md = null;

        try {

          md = MessageDigest.getInstance("MD5");
          md.update(input.getBytes());

          byte[] byteData= md.digest();

          String digested = DatatypeConverter.printHexBinary(byteData).toUpperCase();

          return digested;

        } catch (NoSuchAlgorithmException e) {
            return null;
        }

    }

    public boolean isValid(String username, String password) {

        String hashedPassword = createHashData(password);

        Optional<Authenticate> entityOpt = repoAuthenticate.finduser(username, hashedPassword);

        return entityOpt.isPresent();

    }

}
